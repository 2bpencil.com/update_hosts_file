# update_hosts_file.py

This is an admin script for automatically updating the hosts file on my system.

## License

Some sort of Creative Commons license, once I figure out which.

## Configuration

Configuration is via the update_hosts_file.config file. This file is also used
to track the last-update date.

## Required Libraries

The only non-standard library update_hosts_file uses is [requests].

## To do
	- Put the config file in the user's home directory.
	- Put the log file in the user's home directory.
	- Update this to use the temp directory as the place to download the new file.
	- Figure out how to get rid of the extra whitespace the HTTP transfer adds.
	  - This might be a Windows-specific issue. I don't think I got any
		extra whitespace on my Mac.
	- Add an "undo" function.
	- Do I want to delete the hosts file I download as a temp file? I think yes.
	- Add a '-v' switch to enable verbose screen output.
	- Done - Make a file of my own mappings to prepend to the downloaded file.

## Author

Written by Bryan Patzke in 2015.

[requests]: http://docs.python-requests.org/en/latest/
