#!/usr/bin/env python

import configparser
import http.client
import logging
import os
import requests
import shutil

CONFIG_FILE = 'update_hosts_file.config'

## Configure logging
log_file_msg_format = '%(levelname)s,%(name)s,%(asctime)s,%(message)s'

logging.basicConfig(filename='update_hosts_file.log', level=logging.INFO,
                    format=log_file_msg_format)
logger = logging.getLogger()
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.WARNING)
console_formatter = logging.Formatter('%(levelname)s - %(name)s - %(message)s')
console_handler.setFormatter(console_formatter)
logger.addHandler(console_handler)

config = configparser.ConfigParser()
config.read(CONFIG_FILE)

http_method = config['DEFAULT']['http_method']
hosts_file_server = config['DEFAULT']['hosts_file_server']
new_hosts_file_name = config['DEFAULT']['new_hosts_file_name']
hosts_file_name = config['DEFAULT']['hosts_file_name']
local_hosts_file_additions = hosts_file_name + '-local'
hosts_file_backup_name = config['DEFAULT']['hosts_file_backup_name']
hosts_file_url = hosts_file_server + new_hosts_file_name
last_modified_date = config['DEFAULT']['last_modified_date']

headers = {'if-modified-since': last_modified_date}

if __name__ == '__main__':
    try:
        response = requests.request(http_method, hosts_file_url, headers=headers)
    except Exception as er:
        logger.error('Bad things happened: %s', er)
        raise SystemExit(-1)

    if response.status_code == requests.codes.ok:
        logger.info('New hosts file found - updating.')

        localhosts = None
        with open(local_hosts_file_additions, 'r') as lh:
            localhosts = lh.read()

        # Write the new hosts file to a temp file.
        with open(new_hosts_file_name, 'w') as hf:
            hf.write(localhosts)
            hf.write(response.text)

        # Copy the existing hosts file to the backup hosts file.
        os.replace(hosts_file_name, hosts_file_backup_name)

        # Copy the new hosts file to the etc directory, overwriting the old file.
        shutil.copyfile(new_hosts_file_name, hosts_file_name)

        config['DEFAULT']['last_modified_date'] = response.headers['last-modified']
        with open(CONFIG_FILE, 'w') as configfile:
            config.write(configfile)

        logger.info('Successfully updated the hosts file.')
    
    elif response.status_code == requests.codes['not_modified']:
        logger.info('Hosts file is up to date.')
    else:
        logger.error('Unable to retrieve hosts file: %d - %s',
                     response.status_code, response.reason)

    response.close()
